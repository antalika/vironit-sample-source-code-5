/**
 * SRServerManager.m
 * SnowRepublicTestApp
 *
 * Created on 11.11.16.
 * Copyright © 2016 vironit. All rights reserved.
 *
 * A small example of using AFNetworking to implement authentication. Receiving a token and saving it.
 */

#import "SRServerManager.h"

/**
 * Logic
 */
#import "SRDataManager.h"
#import "SRAccountRealm.h"

static NSString *stringWithURL = @"http://myhome.ch/api/v1/";

@implementation SRServerManager
/**
 * @return {instancetype} sharedManager
 */
+ (instancetype)sharedManager {
	static dispatch_once_t once;
	static SRServerManager *sharedManager;
	dispatch_once(&once, ^{
		sharedManager = [[self alloc] initWithBaseURL:[NSURL URLWithString:stringWithURL]];
		sharedManager.responseSerializer = [AFHTTPResponseSerializer serializer];
		sharedManager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
	});
	
	return sharedManager;
}

/**
 * @param {NSString *}         login          
 * @param {NSString *}         password       
 * @param {NSString *}         installationId 
 * @param {NSString *}         companyId      
 * @param {NSString *}         checkAdmin     
 * @param {void     (^)(void)} succes         
 * @param {void     (^)(NSError *)}           failure    
 */
- (void)getTokenForUsername:(NSString *)login
				andPassword:(NSString *)password
		  andInstallationId:(NSString *)installationId
			   andCompanyId:(NSString *)companyId
			  andCheckAdmin:(NSString *)checkAdmin
					 succes:(void (^)(void))succes
					failure:(void (^)(NSError *))failure {
	
	NSDictionary *parameters = @{@"username" : login, @"password" : password, @"companyId" : @"1", @"installationId" : @"1", @"locale" : @"en"};
	
	[self POST:@"Login" parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
		NSError* errorJson;
		NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
															 options:kNilOptions
															   error:&errorJson];
		if (!errorJson) {
			NSLog(@"%@", json);
			NSDictionary *dataDictionary = json[@"data"];
			NSString *token = dataDictionary[@"token"];
			if (token) {
				[[SRDataManager sharedManager] writeLogin:login andPassword:password andToken:token];
				
				succes();
			}
			else {
				NSLog(@"Error - get token");
				NSError * errorToken = [NSError errorWithDomain:@"" code:1 userInfo:nil];
				failure(errorToken);
			}
		}
		else {
			
			failure(errorJson);
		}
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		
		failure(error);
	}];
}

/**
 * @param {NSString *}         locale
 * @param {void     (^)(void)} succes
 * @param {void     (^)(NSError *)}   failure      
 */
- (void)getAllInfoForLocale:(NSString *)locale
					succes:(void (^)(void))succes
				   failure:(void (^)(NSError *))failure {
	
	SRAccountRealm *accountRealm = [[SRAccountRealm allObjects] firstObject];
	NSString *token = accountRealm.token;
	if (!token) {
		return;
	}
	
	NSDictionary *parameters = @{@"token" : token, @"locale" : locale};
	
	[self POST:@"All" parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
		NSError* errorJson;
		NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
															 options:kNilOptions
															   error:&errorJson];
		if (!errorJson) {			
			[[SRDataManager sharedManager] writeNews:json];
			
			succes();
		}
		else {
			
			failure(errorJson);
		}

	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		failure(error);
	}];
}


@end
